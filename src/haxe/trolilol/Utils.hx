package trolilol;

import etwin.flash.MovieClip;

import hf.Hf;
import hf.Entity;

class Utils {

  public static function getTextsStartingAt(hf: Hf, start: Int): Array<String> {
    var texts = [];
    while (true) {
      var text = hf.Lang.get(start++);
      if (text == null) {
        return texts;
      } else {
        texts.push(text);
      }
    }
  }

  public static function makeDatTorch(hf: Hf, target: Entity): MovieClip {
    var datTorch = target.game.depthMan.attach("$torch", hf.Data.DP_SPRITE_TOP_LAYER);
    datTorch._x = Std.random(hf.Data.GAME_WIDTH);
    datTorch._y = Std.random(hf.Data.GAME_HEIGHT);
    datTorch._xscale = Std.random(500);
    datTorch._yscale = Std.random(500);
    target.stick(datTorch, 0, -80);
    target.fl_stickRot = true;
    target.setElaStick(0.03);
    return datTorch;
  }

  public static inline function forceParticles(hf: Hf, particlesMax: Int, fun: Void -> Void): Void {
    var oldDetail = hf.GameManager.CONFIG.fl_detail;
    var oldParticles = hf.Data.MAX_FX;
    hf.Data.MAX_FX = particlesMax;
    hf.GameManager.CONFIG.fl_detail = true;
    fun();
    hf.Data.MAX_FX = oldParticles;
    hf.GameManager.CONFIG.fl_detail = oldDetail;
  }
}
