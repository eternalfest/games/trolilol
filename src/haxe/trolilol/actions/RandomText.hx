package trolilol.actions;

import hf.Hf;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class RandomText implements IAction {
  private static var SUB_TEXTS_START(default, never): Int = 220;

  public var isVerbose(default, null) = false;
  public var name(default, null) = Obfu.raw("randomText");
  private var texts: Null<Array<String>> = null;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var text = ctx.getGame().fxMan.attachAlert(this.getText(ctx.getHf()));
    text._x = Std.random(400);
    text._y = Std.random(500);
    text._xscale = 50;
    text._yscale = 50;
    return false;
  }

  private function getText(hf: Hf): String {
    if (this.texts == null)
      this.texts = Utils.getTextsStartingAt(hf, SUB_TEXTS_START);

    return this.texts[Std.random(this.texts.length)];
  }
}