package trolilol.actions;

import patchman.Patchman;
import patchman.IPatch;
import etwin.Obfu;
import hf.Hf;

import merlin.IAction;
import merlin.IActionContext;

class Flip implements IAction {
  
  public var isVerbose(default, null) = false;
  public var name(default, null) = Obfu.raw("flip");

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var flipX = ctx.getBoolOr(Obfu.raw("x"), false);
    var flipY = ctx.getBoolOr(Obfu.raw("y"), false);

    game.flipX(flipX);
    game.flipY(flipY);
    return false;
  }
}
