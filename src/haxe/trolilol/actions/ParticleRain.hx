package trolilol.actions;

import patchman.Patchman;
import patchman.IPatch;
import etwin.Obfu;
import hf.Hf;
import hf.FxManager;

import merlin.IAction;
import merlin.IActionContext;

class ParticleRain implements IAction {

  private static var TEXTS_START(default, never): Int = 220;
  
  public var isVerbose(default, null) = false;
  public var name(default, null) = Obfu.raw("particleRain");

  private var texts: Null<Array<String>> = null;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var hf = ctx.getHf();
    var game = ctx.getGame();

    Utils.forceParticles(hf, 30, function() {
      for (p in game.getPlayerList()) {
        var pts = Std.random(2000000) - 1000000;
        var pos = { x: p.x + Std.random(50) - 25, y: p.y + Std.random(50) - 25 };
        p.getScore(cast pos, pts);

        for (_ in 0...3) {
          game.fxMan.inGameParticles(hf.Data.PARTICLE_RAIN, Std.random(hf.Data.GAME_WIDTH), 0, 1);
        }

        game.fxMan.inGameParticles(hf.Data.PARTICLE_SPARK, Std.random(hf.Data.GAME_WIDTH), Std.random(hf.Data.GAME_HEIGHT), 3);
      }
    });

    return false;
  }

  private function getText(hf: Hf): String {
    if (this.texts == null) {
      this.texts = Utils.getTextsStartingAt(hf, TEXTS_START);
    }

    return this.texts[Std.random(this.texts.length)];
  }

  private function displayText(hf: Hf, fxMan: FxManager, str: String): Void {
    var text = fxMan.game.depthMan.attach("hurryUp", hf.Data.DP_INTERF);
    text._x = Std.random(hf.Data.GAME_WIDTH);
    text._y = Std.random(hf.Data.GAME_HEIGHT);
    Reflect.setField(text, "label", str);
    text._xscale = 50;
    text._yscale = 50;
    fxMan.mcList.push(text);
    fxMan.lastAlert = text;
  }
}
