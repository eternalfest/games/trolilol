package trolilol.actions;

import patchman.Patchman;
import patchman.IPatch;
import etwin.Obfu;
import etwin.flash.MovieClip;
import hf.Hf;
import hf.mode.GameMode;
import hf.entity.Player;

import merlin.IAction;
import merlin.IActionContext;
import custom_clips.CustomClips;

class SpawnTrackingVortex implements IAction {

  public var isVerbose(default, null) = false;
  public var name = Obfu.raw("spawnTrackingVortex");

  private var customClips: CustomClips;

  public function new(customClips: CustomClips) {
    this.customClips = customClips;
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var pid = ctx.getInt(Obfu.raw("pid"));
    for (p in game.getPlayerList()) {
      var vortex = TrackingVortex.attach(this.customClips, game, Std.random(400), 800, p, pid);
    }

    return false;
  }

}

@:hfTemplate
class TrackingVortex extends hf.Entity {

  private var target: Player;
  private var speed: Float = 5;
  private var pid: Int;
  private var hasAttachedBg: Bool = false;

  @:keep
  private function new() {
    super(null);
  }

  public static function attach(customClips: CustomClips, g: GameMode, x: Float, y: Float, target: Player, pid: Int): TrackingVortex {
    var self: TrackingVortex = customClips.attach(g.depthMan, "hammer_portal", g.root.Data.DP_SPRITE_BACK_LAYER, TrackingVortex.getClass(g.root));
    self.init(g);
    self.target = target;
    self.x = x;
    self.y = y;
    self.pid = pid;
    return self;
  }

  override public function update(): Void {
    var dist = this.target.distance(this.x, this.y);
    if(dist < 10) {
      this.game.usePortal(this.pid, null);
    } else {
      if (!this.hasAttachedBg && dist < 180) {
        this.hasAttachedBg = true;
        this.game.fxMan.attachBg(game.root.Data.BG_GUU, null, 9999);
      }

      var angle = Math.atan2(this.target.y - this.y, this.target.x - this.x);
      this.x += Math.cos(angle) * this.speed;
      this.y += Math.sin(angle) * this.speed;
      super.update();
    }
  }
}
