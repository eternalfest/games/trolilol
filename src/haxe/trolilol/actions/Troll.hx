package trolilol.actions;

import patchman.Patchman;
import patchman.IPatch;
import etwin.Obfu;
import hf.Hf;

import merlin.IAction;
import merlin.IActionContext;

import trolilol.Utils;

class Troll implements IAction {

  private static var TEXTS_START(default, never): Int = 200;
  
  public var isVerbose(default, null) = false;
  public var name(default, null) = Obfu.raw("troll");

  private var texts: Null<Array<String>> = null;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var hf = ctx.getHf();
    var game = ctx.getGame();

    game.fxMan.attachAlert(this.getText(hf));
    Utils.forceParticles(hf, 30, function() {
      for (p in game.getPlayerList()) {
        var datTorch = Utils.makeDatTorch(hf, p);
        game.fxMan.inGameParticles(hf.Data.PARTICLE_BUBBLE, datTorch._x, datTorch._y, 5);
      }
    });

    return false;
  }

  private function getText(hf: Hf): String {
    if (this.texts != null) {
      return this.texts[Std.random(this.texts.length)];
    }

    this.texts = Utils.getTextsStartingAt(hf, TEXTS_START + 1);
    return hf.Lang.get(TEXTS_START);
  }
}
