package trolilol.actions;

import patchman.Patchman;
import etwin.Obfu;
import hf.Hf;

import merlin.IAction;
import merlin.IActionContext;

import trolilol.Utils;

class BreakGame implements IAction {

  public var isVerbose(default, null) = false;
  public var name = Obfu.raw("breakGame");

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var hf = ctx.getHf();
    var game = ctx.getGame();
    
    game.fxMan.attachAlert(hf.Lang.get(240));

    Utils.forceParticles(hf, 30, function() {
      for (p in game.getPlayerList()) {
        Utils.makeDatTorch(hf, p);
        game.fxMan.inGameParticles(hf.Data.PARTICLE_SPARK, Std.random(hf.Data.GAME_WIDTH), Std.random(hf.Data.GAME_HEIGHT), 15);
      }
    });

    hf.Data.GAME_WIDTH = 420 + Std.random(150) - 75;
    hf.Data.GAME_HEIGHT = 500 + Std.random(150) - 75;
    hf.Data.CASE_WIDTH = Std.int(Math.max(12, Std.random(40)));
    hf.Data.CASE_HEIGHT = Std.random(20) + 10;

    return false;
  }

}
