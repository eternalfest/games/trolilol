package trolilol;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.entity.fx.Particle;
import hf.Hf;
import hf.mode.GameMode;

import merlin.IAction;
import custom_clips.CustomClips;

@:build(patchman.Build.di())
class Trolilol {

  @:diExport
  public var patches(default, null): FrozenArray<IPatch> = FrozenArray.of(
    // Changing tile skins
    Ref.auto(hf.levels.View.getTileSkinId).replace(function(hf, id) {
      return Std.random(62) + 1;
    }),
    Ref.auto(hf.levels.View.getColumnSkinId).replace(function(hf, id) {
      return Std.random(62) + 1;
    }),

    // Teleporters teleport randomly
    Ref.auto(hf.levels.GameMechanics.getNextTeleporter).replace(function(hf, self, td) {
      td = cast {
        dir: hf.Data.HORIZONTAL,
        centerX: hf.Entity.x_ctr(Std.random(hf.Data.LEVEL_WIDTH)),
        centerY: hf.Entity.x_ctr(Std.random(hf.Data.LEVEL_HEIGHT)),
      };
      return { fl_rand: true, td: td };
    }),

    // Player config
    Ref.auto(hf.entity.Player.init).after(function(hf, self, game) {
      self.initialMaxBombs = 6;
      self.maxBombs = self.initialMaxBombs;
      self.lives = 0;
      hf.Data.EXTRA_LIFE_STEPS = [];
    }),

    // Fix up progressive lag on spawning+despawning too many entities (due to particles) too many times.
    // Trigger has the trigger list per cell, however those are not removed on the entity destroy (for all
    // entities). Thus getting bigger and bigger, mainly causing tRem (which loops over it) to get slower and
    // slower.
    // That part is fixed by the MEMORY_LEAKS bugfix. However it seems it's not enough and there's some other
    // way garbage pile up in triggers. So also loop and remove anything that is already destroyed (checks
    // that by _name property, not sure 100% correct, but close enough for Trolilol).
    Ref.auto(GameMode.main).after(function(hf: Hf, self: GameMode): Void {
        for (cx in 0...hf.Data.LEVEL_WIDTH) {
            for (cy in 0...hf.Data.LEVEL_HEIGHT) {
                var triggers = self.world.triggers[cx][cy];
                var i = 0;
                while (i < triggers.length) {
                    if (triggers[i]._name == null)
                        triggers.splice(i, 1);
                    else
                        ++i;
                }
            }
        }
    })

    // To give an idea, this is the number of entities in total stored in world.triggers after 3 minutes of
    // trolilol with(out) the above modifications:
    // Without the bugfix and cleanup loop:          22908
    // With the bugfix and without the cleanup loop:  4209
    // With both:                                       75

    // Note that the cleanup loop should be enough, but it's more fixing the sypmtoms than the fixing the
    // cause, so there is still a need to investigate and find the other "triggers leaks" sources in the base
    // code and add the fix for those to Bugfix.
  );

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  public function new(customClips: CustomClips) {
    actions = FrozenArray.of(
      new trolilol.actions.Troll(),
      new trolilol.actions.ParticleRain(),
      new trolilol.actions.Flip(),
      new trolilol.actions.BreakGame(),
      new trolilol.actions.RandomText(),
      new trolilol.actions.SpawnTrackingVortex(customClips)
    );
  }
}
