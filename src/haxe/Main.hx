import patchman.Patchman;
import patchman.IPatch;
import hf.Hf;

import bugfix.Bugfix;
import merlin.Merlin;

import trolilol.Trolilol;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(bugfix: Bugfix, trolilol: Trolilol, merlin: Merlin, patches: Array<IPatch>, hf: Hf) {
    Patchman.patchAll(patches, hf);
  }
}
